# Iot WebAPP

## Descripción de la WepAPP

Se creó un modulo __dashboard__ para cargar la vista __home__ en la cual se encuentran las tablas de los sensores.
Cada una de las tarjetas que muestra las lecturas de los sensores se hicieron creando un modulo llamada __sensor-card__ el cual cuenta con un __display__ y una __chart__. 

Debido a que se llegaban a desplegar una alta cantidad de datos en la grafica y esto alentaba el funcionamiento se decidio por no mostrar todos los datos en el periodo de tiempo seleccionado. En vez de mostrar todos los datos se tomó una muestra normal con 95% de confianza, esto redujo la carga de datos sin perder generalidad de la grafica.


## Credenciales

 usuario: PruebasIot

 contraseña: UptPD74PA

## Requerimientos

* Ejecutar el comando  `npm install`.

## Comandos

* Para iniciar el servicio `ng serve --open`.

# Estructura  de la WeApp.

* __dashboard__- Aquí se encuentran todo los modulos usados para crear las tarjetas de los sensores y las vista home que es en donde se muetran.
* __guards__- Aquí se encuentran los servicios implementados para proteger las paginas.
* __login__- Es la vista de acceso, el servicio se encuentra en services.
* __services__- Aquí se encuentran archivos con los servicios para consumir la API y el socket.io.
* __.gitignore__- Aquí colocamos el listado de carpetas y archivos que no deben estar en el repo.
* __config.js__- Aquí colocamos las variables que se modifican en cada servidor, como la DB y puertos utilizados.
* __package.json__- Metadatos usuales, relevantes para el proyecto.
* __README.md__- Este documento.
la API.
