import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, pipe, throwError } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import decode from 'jwt-decode';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private message: string;

  constructor(
    private _router: Router, 
    private http: HttpClient) { }

  clear(): void {
    localStorage.clear();
  }


  isAuthenticated(): boolean {
    return localStorage.getItem('token') != null && !this.isTokenExpired();
  }

  isTokenExpired(): boolean {
    return false;
  }


  login(username: string, password: string): any {
    return this.http.post<any>(`${environment.apiUrl}/authenticate`, { username: username, password: password })
      .pipe(map(res => {
        if (res && res.result.token) {
          localStorage.setItem('token', res.result.token);
        }

        return res.result;
      }));
  }

  logout(): void {
    this.clear();
    this._router.navigate(['/login']);
  }

  decode() {
    return decode(localStorage.getItem('token'));
  }
}