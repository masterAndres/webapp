import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { environment } from 'src/environments/environment';

import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class SocketService {



  public socket: any;
  private id: any = null;
  constructor(

  ) { }

  connect() {
    if (!this.id) {
      this.socket = io(environment.socketUrl, {});
      this.socket.on('connect', x => {
        console.log("socket connected");
        console.log(this.socket.id)
      });
    }

  }

  on(eventName, callback) {
    this.socket.on(eventName, x => {
      callback(x);
    });
  }

  getData() {
    let observable = new Observable(observer => {
      this.socket.on('dashboard/sensors', (data) => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    })
    return observable;
  }

}
