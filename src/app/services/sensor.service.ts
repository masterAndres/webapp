import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})


export class SensorService {

  private headers:any = new HttpHeaders({
    'token': localStorage.getItem('token')
  })

  constructor(
    private _router: Router,
    private http: HttpClient) { }

  getSensors():any{
    return this.http.get<any>(`${environment.apiUrl}/sensor/all`,{headers:this.headers})
    .pipe(map(res => {
      return res.result;
    }),
    catchError(this.handleError));

  }

  getReads(data):any{
    return this.http.post<any>(`${environment.apiUrl}/sensor/get`,data,{headers:this.headers})
    .pipe(map(res => {
      return res.result;
    }),
    catchError(this.handleError));
  }

  handleError(error):any{
    if(error.error && error.error.error=='InvalidToken'){
      localStorage.clear();
      this._router.navigate(['/login']);
    }
  }
}
