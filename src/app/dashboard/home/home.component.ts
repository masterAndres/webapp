import { Component, OnInit, ViewChildren } from '@angular/core';
import { SensorService } from 'src/app/services/sensor.service';
import { first, map } from 'rxjs/operators';
import { SocketService } from 'src/app/services/socket.service';
import { never } from 'rxjs';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers:[SocketService]
})
export class HomeComponent implements OnInit {

  public sensors:any = [];
  messages = [];
  connection;
  message;
  
  constructor(
    private sensorService: SensorService,
    public socketservice: SocketService
  ) { }

  ngOnInit() {
    this.getSensors();
    this.initSocket();

  }

  getSensors(){
    this.sensorService.getSensors()
    .pipe(first())
    .subscribe(
      data=>{
        data.items.forEach(x => {
          this.sensors.push({
            id:x.sensorId,
            name:x.sensor=="TEMP"?"TEMPERATURA":"HUMEDADA RELATIVA",
            unit:x.sensor=="TEMP"?"°C":"%"
          });
        });
      }
    )
  }

  initSocket() {
    this.socketservice.connect();
    this.connection = this.socketservice.getData().subscribe(x => {
      if(x != null && "sensorId" in x && "value" in x && "createdOn" in x){
        this.sensors.forEach(s => {
          if(s.id==x["sensorId"]) s['last']={value:x["value"],time:x["createdOn"]}
        });
      }
    })
  }


}
