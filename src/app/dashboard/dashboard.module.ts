import { NgModule } from '@angular/core';
import { HomeComponent } from './home/home.component';
import { LayoutComponent } from './layout/layout.component';
import { dashboardRoutes } from './dashboard.routes';
import { RouterModule } from '@angular/router';
import { AuthGuard } from '../guards/auth-guard.service';
import { DisplayComponent } from './display/display.component';
import { ChartComponent } from './chart/chart.component';
import { SensorCardComponent } from './sensor-card/sensor-card.component';
import { BrowserModule } from '@angular/platform-browser';




@NgModule({
  declarations: [
    HomeComponent, 
    LayoutComponent,
    DisplayComponent,
    ChartComponent,
    SensorCardComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forChild(dashboardRoutes)
  ],
  providers:[
    AuthGuard
  ]
})
export class DashboardModule { }
