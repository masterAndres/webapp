import { Component, OnInit, Input } from '@angular/core';
import { Chart } from 'chart.js'
import { SensorService } from 'src/app/services/sensor.service';
import { first, map } from 'rxjs/operators';
import { SensorCardComponent } from '../sensor-card/sensor-card.component';


@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit {

  public LineChart: any = null;
  public minutesArray: any = [15, 30, 60, 120, 300];
  public minutesSelected: number = 15;
  public last: any = { value: 0, time: "" };
  private countDownTimeRef :any=null;

  @Input() Chart: any = null;



  constructor(
    private sensorService: SensorService,
    public sensorcard: SensorCardComponent
  ) { }

  ngOnInit() {
    this.updateChartPlugin();
  }

  ngAfterViewInit() {
    var ctx = document.getElementById(this.Chart.id);
    this.LineChart = new Chart(ctx, {
      type: 'line',
      data: {
        datasets: [{
          label: '',
          data: [],
          fill: false,
          lineTension: 0.2,
          backgroundColor: '#168ede',
          borderColor: '#168ede',
          borderWidth: 4
        }]
      },
      options: {
        elements: {
          point: {
            radius: 0
          }
        },
        responsive: true,
        maintainAspectRatio: false,
        title: {
          text: "Titulo",
          display: false,
        },
        legend: {
          display: false
        },
        scales: {
          yAxes: [{
            position: 'right',
            display: true,
            ticks: {
              beginAtZero: true
            },
            gridLines: {
              color: 'black'
            }
          }],
          xAxes: [{
            type: 'time',
            display: true,
            scaleLabel: {
              display: false,
              labelString: 'Date'
            },
            gridLines: {
              color: 'black'
            }
          }],
        },
        chartArea: {
          backgroundColor: '#808080'
        },
      }
    });

    setTimeout(() => {
      this.getReads();
    }, 1000);

  }

  private updateChartPlugin() {
    Chart.pluginService.register({
      beforeDraw: function (chart) {
        if (chart.config.options.chartArea && chart.config.options.chartArea.backgroundColor) {
          var ctx = chart.chart.ctx;
          var chartArea = chart.chartArea;

          ctx.save();
          ctx.fillStyle = chart.config.options.chartArea.backgroundColor;
          ctx.fillRect(chartArea.left, chartArea.top, chartArea.right - chartArea.left, chartArea.bottom - chartArea.top);
          ctx.restore();
        }
      }
    });
  }

  private onSelect(event: any) {
    this.minutesSelected = event.target.value;
    this.getReads();

  }

  private showData() {
    if (this.LineChart) {
      this.LineChart.update();
      this.sensorcard.updateDisplay(this.last);
      this.updateSensor();
    }
  }

  public getReads() {
    this.clearTimeOut();
    this.sensorService.getReads({ id: this.Chart.id, min: Number(this.minutesSelected) })
      .pipe(first())
      .subscribe(
        data => {
          if (data.totalFound) {
            this.LineChart.data.datasets[0].data = [];
            data.items.forEach((x, i) => {
              this.LineChart.data.datasets[0].data.push({ x: x.createdOn, y: x.value });
              if (i == data.totalFound - 1) {
                this.last = { value: x.value, time: x.createdOn };
                this.showData();
              }
            });
          }
        }
      )
  }

  private updateSensor() {
    this.clearTimeOut();
    if (this.last.value != this.sensorcard.Sensor.last.value) {
      if (this.last.time != this.sensorcard.Sensor.last.time) {
        this.last = this.sensorcard.Sensor.last;
        this.LineChart.data.datasets[0].data.pop();
        this.LineChart.data.datasets[0].data.push({ x: this.last.time, y: this.last.value});
        this.showData();

      }
    }

    this.countDownTimeRef = setTimeout(() => {
      this.updateSensor();
    }, 2000);
  }
  

  private clearTimeOut(){
    if(this.countDownTimeRef) clearTimeout(this.countDownTimeRef);
  }

}
