import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-display',
  templateUrl: './display.component.html',
  styleUrls: ['./display.component.scss']
})
export class DisplayComponent implements OnInit {

/*   @Input() type:string = null;
  @Input() unit:string = null;
  @Input() value:number = 0;
  @Input() time:Date = null; */
  @Input() Display:any = null;
  constructor() { }

  ngOnInit() {
  }

}
