import { Component, OnInit, Input, ViewChild, AfterViewInit } from '@angular/core';
import { ChartComponent } from '../chart/chart.component';
import { first } from 'rxjs/operators';
import { DisplayComponent } from '../display/display.component';

@Component({
  selector: 'app-sensor-card',
  templateUrl: './sensor-card.component.html',
  styleUrls: ['./sensor-card.component.scss']
})
export class SensorCardComponent implements OnInit, AfterViewInit {


/*   public idchart:string = null;
  public name:string = null;
  public unit:string = null; */
  public display:any =null;
  public chart:any =null;
  /* public _reads:Array<number> = [];
  public _last:number = 0; */

  @Input() Sensor:any = null;

  /* @Input() reads:Array<number> = []; */

  constructor(
  ) { }

  ngOnInit() {
    this.updateId();
  }

  ngAfterViewInit(){

  }



  updateDisplay(data){
    this.display['value']=data.value.toFixed(2);
    this.display['time']=this.convertDate(data.time);
  }

  updateId(){
    this.display={
      name:this.Sensor.name,
      unit:this.Sensor.unit
    }
    this.chart={
      id:this.Sensor.id
    }
  }

  convertDate(inputFormat) {
    function pad(s) { return (s < 10) ? '0' + s : s; }
    var d = new Date(inputFormat);
    return [pad(d.getHours()), pad(d.getMinutes()), pad(d.getSeconds())].join(':')+' '+[pad(d.getDate()), pad(d.getMonth()+1), d.getFullYear()].join('/');
  }


}
